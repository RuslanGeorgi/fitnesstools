from django.contrib.auth.forms import UserCreationForm

from accounts.models import User


class AccountRegistrationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = User
        # fields = ['username', 'first_name', 'last_name', 'email']
