from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView

from accounts.forms import AccountRegistrationForm
from accounts.models import User


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    # success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm
