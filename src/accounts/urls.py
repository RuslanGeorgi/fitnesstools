from django.urls import path

from accounts.views import AccountRegistrationView

app_name = 'accounts'

urlpatterns = [
    path('registration/', AccountRegistrationView.as_view(), name='registration')
]
