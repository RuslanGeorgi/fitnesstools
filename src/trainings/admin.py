from django.contrib import admin

from trainings.models import Training

admin.site.register(Training)
