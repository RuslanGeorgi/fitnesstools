from django.db import models

from accounts.models import User


class Training(models.Model):
    start_of_training = models.DateTimeField(null=False)
    end_of_training = models.DateTimeField(null=True)
    client = models.ForeignKey(to=User, related_name='trainings', on_delete=models.CASCADE)
